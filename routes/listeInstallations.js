var express = require('express');
var router = express.Router();
var db = require('../model/db');
var listeCollections = require('../controller/installations');

// route /installations retourne la liste de toutes les installations dans la base de donn�es
router.get('/installations', function (req, res) {

    var nomArrondissement = req.query['arrondissement'] || 'default';
    // si aucun param�tre n'�tait fourni, la liste de toutes les installations est retourn�e
    if (nomArrondissement != 'default') {
        listeCollections.trouverInstallations(function (listeIstallations) {
            if (listeIstallations.length === 0)
                res.sendStatus(400);
            else {
                var listeIstallationsChoisi = trouverInstallationsParNom(nomArrondissement, listeIstallations);
                if (listeIstallationsChoisi.length === 0)
                    res.sendStatus(400);
                else
                    res.status(201).json(listeIstallationsChoisi);
            }
        });
    } else {
        listeCollections.trouverInstallations(function (listeIstallations) {
            if (listeIstallations.length === 0)
                res.sendStatus(400);
            else {
                res.status(201).json(listeIstallations);
            }
        });
    }
});

function trouverInstallationsParNom(nomArrondissement, listeIstallations){
    var listeIstallationsChoisi = [];
    var tailleListe = listeIstallations.length;

    for (var i = 0; i < tailleListe; i++) {

        if (listeIstallations[i].hasOwnProperty('NOM') && listeIstallations[i].ARRONDISSE == nomArrondissement)
            listeIstallationsChoisi.push(listeIstallations[i]);
        else if (listeIstallations[i].hasOwnProperty('nom') && listeIstallations[i].arrondissement.nom_arr == nomArrondissement)
            listeIstallationsChoisi.push(listeIstallations[i]);
        
    }
    return listeIstallationsChoisi;
}

module.exports = router;
