
exports.insererDB =  function (nomCollection, data) {

    db.collection(nomCollection, function (err, collection) {
        if (err) {
            console.log("Erreur de la base de donnees: \n" + err);
            db.close();
        }
        else {
            // on supprime les anciennes donn�es avant d'inserer des nouvelles
            collection.remove({}, function (err) {
                if (err) console.log(err);
                collection.insert(data, function (err) {
                    if (err) {
                        console.log("Erreur: " + err);
                        db.close();
                    }
                    else {
                        console.log("La collection " + nomCollection + " est cree ");
                    }
                });
            });
        }

    });
}
