// formattage de donn�es sur les glissades et les patinoires selon leurs schemas

exports.formateurJson = function (liste, typeAire){
    var nouveauListe = new Array();
    var objet;

    for (var i = 0; i < liste.length; i++) {

        objet = initialiserObjetXml(liste[i], typeAire);
        nouveauListe.push(objet);
    }
    return nouveauListe;
}

function initialiserObjetXml(objet, typeAire) {
 
    var arrondissement = {
        "nom_arr": objet.arrondissement[0].nom_arr[0],
        "cle": objet.arrondissement[0].cle[0],
        "date_maj": objet.arrondissement[0].date_maj[0]
    };

    var objetInitialise;

    if (typeAire == "glissades") {
        objetInitialise = {
            "nom": objet.nom[0],
            arrondissement,
            "ouvert": objet.ouvert[0],
            "deblaye": objet.deblaye[0],
            "condition": objet.condition[0]
        };
    }
    else {
        objetInitialise = {
            "nom": objet.nom[0],
             arrondissement,
            "ouvert": objet.ouvert[0],
            "deblaye": objet.deblaye[0],
            "arrose": objet.arrose[0],
            "resurface": objet.resurface[0],
            "condition": objet.condition[0]
        };
    }

    return objetInitialise;
}
