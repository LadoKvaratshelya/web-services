var express = require('express');
var router = express.Router();
var raml2html = require('raml2html');

router.get('/doc', function (req, res) {

    var configuration = raml2html.getDefaultConfig(false);

    raml2html.render("routes/doc/documentation.raml", configuration).then(function (data) {

        res.send(data);

    }, function (err) {
        console.log(err);
        res.sendStatus(500);
    });
});

module.exports = router;
