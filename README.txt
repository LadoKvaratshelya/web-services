Auteur: Vladimir Kvaratshelya
Code permanent: KVAV23027407

Le but de projet est de r�cup�rer les donn�es sur les installations pour faire des activit�s sportives, 
disponibles � la publique et offertes par la ville de Montr�al.

Le projet est accessible sur Bitbucket: https://bitbucket.org/LadoKvaratshelya/web-services

- Les fonctionnalit�s XP r�alis�es:

A1 - 15 xp
A2 - 5  xp
A3 - 5  xp
A4 - 10 xp
A5 - 10 xp
A6 - 10 xp
D1 - 15 xp
D2 - 5  xp
D4 - 20 xp
D3 - 20 xp


- Les technologies utilis�es dans l'application:
Node.js 8.9.3
Express.js 4.15.5
MongoDB 3.4.9

-Installation:

  Installer node.js et MongoDb, apres on installe toutes les d�pendances:

  npm install "d�pendance"

- Ex�cution:

  Ouvrir un terminal et d�marrer le serveur de base de donn�es MongoDb:

  mongod

  Ouvrir un autre terminal et executer:
  
  la commande npm start 
  ou
  la commande nodemon 

  Maintenant on peut acc�der aux fonctionnalit�s de l'application.

- Les interfaces graphiques :

  Affichage et modification de services:
  http://localhost:3000
  
  Documentation sur les services Rest:
  http://localhost:3000/doc
  
