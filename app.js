var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var Strategy = require('passport-twitter').Strategy;

passport.use(new Strategy({
    consumerKey: 'KCxPrqWigHhXRLw7IIHulFNq2',
    consumerSecret: '2iGFpXmoI2NyvYfUA0jxMtlCTruxCbPelCsyppQCFDwqXkfanm',
    callbackURL: 'http://localhost:3000/twitter/return'
}, function (token, tokenSecret, profile, callback) {
    return callback(null, profile);
}));

passport.serializeUser(function (user, callback) {
    callback(null, user);
});

passport.deserializeUser(function (obj, callback) {
    callback(null, obj);
});

var app = express();

var index = require('./routes/index');
var users = require('./routes/users');
var importDonnees = require('./routes/importationDonnees');
var listeInstallations = require('./routes/listeInstallations');
var modification = require('./routes/modification');
var suppression = require('./routes/suppression');
var docs = require('./routes/documents');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', importDonnees); // A1
app.use('/', docs); // A3
app.use('/', listeInstallations); // A4
app.use('/', modification); // D1
app.use('/', suppression); // D2

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
