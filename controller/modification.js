var jsonschema = require('jsonschema');
var schemas = require('../routes/schemas'); 
var mongo = require('mongodb');

exports.modifierObjet = function (req, nomCollection, idObjet, callback) {
    var result;

    if (nomCollection == "glissades")
        result = jsonschema.validate(req.body, schemas.schemaGlissade);
    else if (nomCollection == "patinoires")
        result = jsonschema.validate(req.body, schemas.schemaPatinoire);
    else
        result = jsonschema.validate(req.body, schemas.schemaPiscine);

    if (result.errors.length > 0) {
        console.log(result.errors);
        callback(400);
    } else {
        db.collection(nomCollection, function (err, collection) {
            if (err) {
                console.log(err);
                callback(500);
            } else {
                collection.update({ _id: new mongo.ObjectId(idObjet) }, { $set: req.body }, function (err, result) {
                    if (err) {
                        console.log(err);
                        callback(500);
                    } else if (result.result.n === 0) {
                        console.log(err);
                        callback(404);
                    } else {
                        callback(200);
                    }
                });
            }
        });
    }
}

exports.supprimerObjet = function (req, nomCollection, idObjet, callback) {

    db.collection(nomCollection, function (err, collection) {
        if (err) {
            console.log(err);
            callback(500);
        } else {
            collection.remove({ _id: new mongo.ObjectId(idObjet) }, function (err, result) {
                if (err) {
                    console.log(err);
                    callback(500);
                } else if (result.result.n === 0) {
                    callback(404);
                } else {
                    callback(200);
                }
            });
        }
    });
}

