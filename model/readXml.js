var parseString = require('xml2js').parseString;
var http = require('http');
var formater = require('./formaterJson.js');
var db = require('./db.js');
var sauvegarde = require('./requetesDB.js');
var iconv = require('iconv-lite');

// conversion de donn�es en json et sauvegarde dans la base de donn�es
function traiterData(err, data) {
    if (err) {
        console.log(err);
    } else {
        var parsedData = JSON.stringify(data, null, 2);
        var jsonObject = JSON.parse(parsedData);
        var liste;
        var typeAire;

        if (jsonObject.hasOwnProperty('glissades')) {
            liste = jsonObject.glissades.glissade;
            typeAire = "glissades";
        }
        else if (jsonObject.hasOwnProperty('patinoires')) {
            liste = jsonObject.patinoires.patinoire;
            typeAire = "patinoires";
        }

        var parsedData = formater.formateurJson(liste, typeAire);

        sauvegarde.insererDB(typeAire, parsedData);
    }
}

// importation de donn�es
exports.importerXml = function importerXml(url) {
    http.get(url, function (res) {
        
        var xmlData = '';
        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            xmlData += chunk.toString('utf8');
        });

        res.on("end", function () {
            parseString(xmlData, traiterData);
        });
    }).on('error', function (err) {
        console.log("Erreur d'importation a l'URL: /n" + url + "/n " + err);
        });
}



















