var db = require('./db.js');
var csv = require("csvtojson");
var http = require('http');
var sauvegarder = require('./requetesDB.js');

// importation de donn�es en format CSV
exports.importerCSV = function importerCSV(url) {

    http.get(url, function (res) {
        
        var chunks = [];
        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var data = chunks.join();

            csv().fromString(data).on("end_parsed", function (jsonArrayObj) {
                sauvegarder.insererDB("piscines", jsonArrayObj);
            });
        });
    }).on('error', function (err) { console.log("Erreur d'importation a l'URL: /n" + url + "/n " + err); });
}





