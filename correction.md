Auteur: Vladimir Kvaratshelya
Code permanent: KVAV23027407


--Fonctionnalité A1:
On importe les données de la ville de Montréal qui étaient obtenues à l'aide de requêtes HTTP
et on les sauvegarde dans la base de données mongodb. Trois collestion de fichiers sont créés
à cet effet: glissades, patinoires et piscines.
Pour l'importation immédiate de données il faut entrer dans le dossier racine du projet et executer le script: node importImmediat.js

 
--Fonctionnalité A2:
L'importation de données du point A1 est faite automatiquement chaque jour à minuit.
Pour tester la fonctionnalité A2:
Le service s'active avec le démarrage de l'application
../route/importationDonnees.js - fonction donnees.importerAMinuit

--Fonctionnalité A3:
La documentation de services Rest en format HTML, généré à partir de fichiers RAML.
Pour tester la fonctionnalité A3: localhost:3000/doc

--Fonctionnalité A4:
Le système offre un service REST permettant d'obtenir la liste des installations pour un arrondissement spécifié en paramètre. 
Les données retournées sont en format JSON. Ex. GET /installations?arrondissement=LaSalle

Si aucun nom de l'arondissement n'était spécifié, la liste de toutes les installations est retournée.
Si le nom de l'arondissement est incorrect, aucune installation n'est retournée.

Pour tester la fonctionnalité A4:
localhost:3000/installations?arrondissement=LaSalle

--Fonctionnalité A5:
Formulaire HTML est disponible sur la page d'accueil du serveur localhost:3000/

Si aucun nom de l'arondissement n'était spécifié, la liste de toutes les installations est retournée.
Si le nom de l'arondissement est incorrect, aucune installation n'est retournée.

Pour tester la fonctionnalité A5:
On choisi le répertoire "Recherche des installations par le nom de l'arrondissement"

--Fonctionnalité A6:
 La liste de toutes les installations est prédéterminée dans une liste déroulante et l'utilisateur choisira une installation parmi cette liste.

 Pour tester la fonctionnalité A6:
 On choisi le répertoire "Recherche des installations dans la liste deroulante"
 Lorsqu'on choisi le nom de l'installation dans la liste déroulante, le ou les tableaux contenants les informations s'affichent par dessous.

--Fonctionnalité D1:
Le système offre un service REST permettant de modifier l'état d'une glissade. Le client doit envoyer
un document JSON contenant les modifications à apporter à la glissade. Le document JSON doit être
validé avec json-schema.

Pour tester la fonctionnalité D1:
Il faut specifier l'id de l'objet glissade et nom de la collection "glissades".
Exemple:
localhost:3000/modifier?id=5a2475fc82781424a4870fb2&collection=glissades


--Fonctionnalité D2:
Le système offre un service REST permettant de supprimer une glissade.

Pour tester la fonctionnalité D2:
Il faut specifier l'id de l'objet glissade et nom de la collection "glissades".
Exemple:
localhost:3000/supprimer?id=5a2475fc82781424a4870fb2&collection=glissades

--Fonctionnalité D4:
Le système offre une procédure d'authentification du type « Login with Twitter » et permet de
restreindre l'accès aux fonctionnalités de modification et suppression de D3 uniquement à un utilisateur
prédéfini.

Pour tester la fonctionnalité D4:
S'authentifier au Twitter.
Si l'authentification est reussie, les champs de fonctionnalité D3 vont s'afficher.

--Fonctionnalité D3:
On peut supprimer ou modifier les glissades retournées par l'outil de recherche.
L'application invoque les services faits en D1 et D2 avec des appels Ajax et affiche une confirmation en cas de succès ou un message d'erreur en cas d'erreur.
La même fonctionnalité est développeé pour les piscines et installations aquatiques.

Pour tester la fonctionnalité D3:
S'authentifier au Twitter.
Entrez ID BD dans le champs ID BD.
Si l'id existe, un tableau contenant l'infomations sur l'objet apparait par dessous et les boutons Modifier et Supprimer apparaissent en dessous de tableau.
On modifie les paramètres de l'objet directement dans le tableau.





































