$(document).ready(function () {

    var nomCollection;
    var installationTrouve = false;

    $("#rechercheDonnees").click(function () {
        
        var idDB = $("#modif").val();
        var url = "/installations";
        // Appel alax pour obtenir la liste de toutes les installations
        $.get(url, function (donnees) {
            trouverInstallation(donnees, idDB);
            mettreAJourBoutons();
        }).fail(function (err) {
            $("#message").html("Erreur; " + err);
        });
    });

    function trouverInstallation(listeInstallations, idDB) {

        for (var i = 0; i < listeInstallations.length; i++) {

            if (listeInstallations[i]._id === idDB.toString().trim()) {
                afficherInfoInstallation(listeInstallations[i]);
                installationTrouve = true;
            }
        }

        // affichage si aucune installation n'�tait trouv�e 
        if (!installationTrouve) {
            $("#message").html("Installation n'existe pas, verifiez ID BD ");
            $("#paramGlisade").hide();
            $("#paramPatinoire").hide();
            $("#paramPiscine").hide();
        }
    }

    function afficherInfoInstallation(installation) {
        if (installation.hasOwnProperty('ID_UEV')) {
            $("#paramGlisade").hide();
            $("#paramPatinoire").hide();
            $("#paramPiscine").show();
            $("#donneesPiscine").html(ajouterLignePiscine(installation));

            nomCollection = "piscines";
        }
        else if (installation.hasOwnProperty('arrose')) {
            $("#paramGlisade").hide();
            $("#paramPiscine").hide();
            $("#paramPatinoire").show();
            $("#donneesPatinoire").html(ajouterLignePatinoire(installation));

            nomCollection = "patinoires";
        }
        else {
            $("#paramPatinoire").hide();
            $("#paramPiscine").hide();
            $("#paramGlisade").show();
            $("#donneesGlissade").html(ajouterLigneGlissade(installation));
            
            nomCollection = "glissades";
        }
    }

    function ajouterLigneGlissade(noeud) {
        var ligne = "<tr><td contenteditable=true>" + noeud.nom + "</td>" +
                        "<td contenteditable=true>" + noeud.arrondissement.nom_arr + "</td>" +
                        "<td contenteditable=true>" + noeud.arrondissement.cle + "</td>" +
                        "<td contenteditable=true>" + noeud.arrondissement.date_maj + "</td>" +
                        "<td contenteditable=true>" + noeud.ouvert + "</td>" +
                        "<td contenteditable=true>" + noeud.deblaye + "</td>" +
                        "<td contenteditable=true>" + noeud.condition + "</td></tr >";
        return ligne;
    }

    function ajouterLignePatinoire(noeud) {
        var ligne = "<tr><td contenteditable=true>" + noeud.nom + "</td>" +
                        "<td contenteditable=true>" + noeud.arrondissement.nom_arr + "</td>" +
                        "<td contenteditable=true>" + noeud.arrondissement.cle + "</td>" +
                        "<td contenteditable=true>" + noeud.arrondissement.date_maj + "</td>" +
                        "<td contenteditable=true>" + noeud.ouvert + "</td>" +
                        "<td contenteditable=true>" + noeud.deblaye + "</td>" +
                        "<td contenteditable=true>" + noeud.arrose + "</td>" +
                        "<td contenteditable=true>" + noeud.resurface + "</td>" +
                        "<td contenteditable=true>" + noeud.condition + "</td></tr >";
        return ligne;
    }

    function ajouterLignePiscine(noeud) {
        var ligne = "<tr><td contenteditable=true>" + noeud.ID_UEV + "</td>" +
                        "<td contenteditable=true>" + noeud.TYPE + "</td>" +
                        "<td contenteditable=true>" + noeud.NOM + "</td>" +
                        "<td contenteditable=true>" + noeud.ARRONDISSE + "</td>" +
                        "<td contenteditable=true>" + noeud.ADRESSE + "</td>" +
                        "<td contenteditable=true>" + noeud.PROPRIETE + "</td>" +
                        "<td contenteditable=true>" + noeud.GESTION + "</td>" +
                        "<td contenteditable=true>" + noeud.POINT_X + "</td>" +
                        "<td contenteditable=true>" + noeud.POINT_Y + "</td>" +
                        "<td contenteditable=true>" + noeud.EQUIPEME + "</td>" +
                        "<td contenteditable=true>" + noeud.LONG + "</td>" +
                        "<td contenteditable=true>" + noeud.LAT + "</td></tr >";
        return ligne;
    }


    $("#modifInstallation").click(function () {

        var idDB = $("#modif").val().toString().trim();

        var url = "/modifier?id=" + idDB + "&collection=" + nomCollection;

        var jsonObj = convertirDonneesEnJson();
        
        // appel ajax pour modifier une installation
        $.ajax({
            url: url,
            method: 'PUT',
            data: jsonObj,
            contentType: "application/x-www-form-urlencoded; charset=utf-8",
            success: function (data, statusText, jqXHR) {
                $("#message").html("Modification reussie ");
            },
            error: function (jqXHR, statusText ,err) {
                $("#message").html("Erreur: " + err);
            }
        });
   });

    $("#supprimerInstallation").click(function () {

        var idDB = $("#modif").val().toString().trim();

        var url = "/supprimer?id=" + idDB + "&collection=" + nomCollection;
        // appel ajax pour supprimer une installation
        $.ajax({
            'url': url,
            'type': 'DELETE',
            'success': function (data) {
                $("#message").html("Suppression reussie ");
            },
            'error': function (err) {
                $("#message").html("Erreur: " + err.responseText);
            },
        });
    });

    function lireDonneesModifies(idTable) {
        var donneesInstallation = [];
        var index = $(idTable).find('tbody').find('td');
        for (var i = 0; i < index.length; i++) {
            var tabValue = $(index[i]).html();
            donneesInstallation.push(tabValue);
        }
        return donneesInstallation;
    }

    function convertirDonneesEnJson() {

        var donneesInstallation = [];
        if (nomCollection === "glissades") {
            donneesInstallation = lireDonneesModifies("table#tGlissade");
            return convertirGlissade(donneesInstallation);
        }
        else if (nomCollection === "patinoires") {
            donneesInstallation = lireDonneesModifies("table#tPatinoire");
            return convertirPatinoire(donneesInstallation);
        }
        else {
            donneesInstallation = lireDonneesModifies("table#tPicsine");
            return convertirPiscine(donneesInstallation);
        }
    }

    //retourne json pour objet glissade
    function convertirGlissade(liste) {
        var arrondissement ={
            nom_arr: liste[1],
            cle: liste[2],
            date_maj: convertirDate(liste[3])
        };

        var objetJson = {
            nom: liste[0],
            arrondissement,
            ouvert: liste[4],
            deblaye: liste[5],
            condition: liste[6]
        };

        return objetJson;
    }

    //retourne json pour objet patinoire
    function convertirPatinoire(liste) {
        var arrondissement = {
            nom_arr: liste[1],
            cle: liste[2],
            date_maj: convertirDate(liste[3])
        };

        var objetJson = {
            nom: liste[0],
            arrondissement,
            ouvert: liste[4],
            deblaye: liste[5],
            arrose: liste[6],
            resurface: liste[7],
            condition: liste[8]
        };
        console.log(objetJson);
        return objetJson;
    }

    //retourne json pour objet piscine
    function convertirPiscine(liste) {
       
        var objetJson = {
            ID_UEV: liste[0],
            TYPE: liste[1],
            NOM: liste[2],
            ARRONDISSE: liste[3],
            ADRESSE: liste[4],
            PROPRIETE: liste[5],
            GESTION: liste[6],
            POINT_X: liste[7],
            POINT_Y: liste[8],
            EQUIPEME: liste[9],
            LONG: liste[10],
            LAT: liste[11]
        };

        return objetJson;
    }

    function convertirDate(date) {
        return date.substring(0, 10) + "T" + date.substring(11, 19) + "Z";
    }

    // afficher les boutons de modification et de suppression si l'objet �tait trouv� et nettoyer le champs de messages
    // cacher les boutons de modification et de suppression si l'objet n'�tait pas trouv� 
    function mettreAJourBoutons() {
        if (installationTrouve) {
            $("#modifInstallation").show();
            $("#supprimerInstallation").show();
            installationTrouve = false;
            $("#message").html("");
        }
        else {
            $("#modifInstallation").hide();
            $("#supprimerInstallation").hide();
        }
    }

});