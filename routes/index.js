var express = require('express');
var router = express.Router();

var session = require('express-session');
var passport = require('passport');
var Strategy = require('passport-twitter').Strategy;

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {user: req.user });
});

router.get('/twitter/login', passport.authenticate('twitter'));

router.get('/twitter/return', passport.authenticate('twitter', {
    failureRedirect: '/'
}), function (req, res) {
    res.redirect('/');
});


module.exports = router;
