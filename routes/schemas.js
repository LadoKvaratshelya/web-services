
module.exports = {
    schemaGlissade: {
        "type": "object",
        "required": true,
        "properties": {
            "nom": {
                "type": "string",
                "required": true
            },
            "arrondissement": {
                "type": "object",
                "required": true,
                "properties": {
                    "nom_arr": {
                        "type": "string",
                        "required": true
                    },
                    "cle": {
                        "type": "string",
                        "required": true
                    },
                    "date_maj": {
                        "type": "string",
                        "format": "date-time",
                        "required": true
                    }
                }
            },
            "ouvert": {
                "type": "int",
                "required": false
            },
            "deblaye": {
                "type": "int",
                "required": false
            },
            "condition": {
                "type": "string",
                "required": true
            }
        }
    },
    schemaPatinoire: {
        "type": "object",
        "required": true,
        "properties": {
            "nom": {
                "type": "string",
                "required": true
            },
            "arrondissement": {
                "type": "object",
                "required": true,
                "properties": {
                    "nom_arr": {
                        "type": "string",
                        "required": true
                    },
                    "cle": {
                        "type": "string",
                        "required": true
                    },
                    "date_maj": {
                        "type": "string",
                        "format": "date-time",
                        "required": true
                    }
                }
            },
            "ouvert": {
                "type": "string",
                "required": false
            },
            "deblaye": {
                "type": "string",
                "required": false
            },
            "arrose": {
                "type": "string",
                "required": false
            },
            "resurface": {
                "type": "string",
                "required": false
            },
            "condition": {
                "type": "string",
                "required": true
            }
        }
    },
    schemaPiscine: {
        "type": "object",
        "required": true,
        "properties": {
            "ID_UEV": {
                "type": "integer",
                "required": true
            },
            "TYPE": {
                "type": "string",
                "required": true
            },
            "NOM": {
                "type": "string",
                "required": true
            },
            "ARRONDISSE": {
                "type": "string",
                "required": true
            },
            "ADRESSE": {
                "type": "string",
                "required": false
            },
            "PROPRIETE": {
                "type": "string",
                "required": false
            },
            "GESTION": {
                "type": "string",
                "required": false
            },
            "POinteger_X": {
                "type": "int",
                "required": true
            },
            "POinteger_Y": {
                "type": "int",
                "required": true
            },
            "EQUIPEME": {
                "type": "string",
                "required": false
            },
            "LONG": {
                "type": "number",
                "required": true
            },
            "LAT": {
                "type": "number",
                "required": true
            },
            
        }
    }
};
