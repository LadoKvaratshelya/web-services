var express = require('express');
var router = express.Router();
var db = require('../model/db');
var modif = require('../controller/modification');

// route /supprimerCollection permet de supprimer une glissade par ID et par nom de collection
router.delete('/supprimer', function (req, res) {

    var idObjet = req.query['id'] || 'default';
    var nomCollection = req.query['collection'] || 'default';
    var status;
    modif.supprimerObjet(req, nomCollection, idObjet, function (status) {
        res.sendStatus(status);
    });
});

module.exports = router;