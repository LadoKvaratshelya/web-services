var express = require('express');
var router = express.Router();
var db = require('../model/db');
var modif = require('../controller/modification');

// route /modifier permet de modifier l'�tat d'un objet par ID et par nom de collection
router.put('/modifier', function (req, res) {

    var idModif = req.query['id'] || 'default';
    var nomCollection = req.query['collection'] || 'default';
    var status;

    if (idModif != 'default')
        modif.modifierObjet(req, nomCollection, idModif, function (status) {
            res.sendStatus(status);
        });
});

module.exports = router;