
exports.trouverInstallations = function (callback) {

    var listeIstallations = [];
    global.nbrCollection = 0;

    db.listCollections().toArray(function (err, collections) {

        if (err) {
            console.log("Erreur de la base de donnees \n" + err);
        }
        else {
            global.totalCollections = collections.length;

            for (var i = 0; i < totalCollections; i++) {

                var nomCollection = collections[i].name;

                db.collection(nomCollection, function (err, collection) {
                    if (err) {
                        console.log("Erreur de la collection \n" + err);
                    }
                    else {
                            trouverInstallationsDansDB(collection, listeIstallations, function (listeIstallations) {
                                callback(listeIstallations);
                            });
                    }
                });
            }
        }
    });
}


function trouverInstallationsDansDB(collection, listeIstallations, callback) {
    collection.find().toArray(function (err, noeuds) {
        if (err) {
            console.log("Erreur lors de la lecture de la collection \n" + err);
            
        } else {
            nbrCollection++;
            for (var j = 0; j < noeuds.length; j++) {
                var noeud = noeuds[j];
                listeIstallations.push(noeud);
            }
        }
        // on arr�te si toutes les collections ont �t� parcouru
        if (nbrCollection == totalCollections) {
            callback(listeIstallations);
        }
    });
}    
