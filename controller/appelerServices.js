var horaire = require('node-schedule');
var lireXml = require('../model/readXml.js');
var lireCSV = require('../model/readCSV.js');

var url1 = 'http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_PATINOIRE.xml';
var url2 = 'http://www2.ville.montreal.qc.ca/services_citoyens/pdf_transfert/L29_GLISSADE.xml';
var url3 = 'http://donnees.ville.montreal.qc.ca/dataset/4604afb7-a7c4-4626-a3ca-e136158133f2/resource/cbdca706-569e-4b4a-805d-9af73af03b14/download/piscines.csv';

exports.importerAMinuit = function () {
    horaire.scheduleJob('00 00 * * *', function () {
 // horaire.scheduleJob('00 22 15 * * *', function () {

        console.log(new Date() + ' Importation automatique de des donnees.');

        lireXml.importerXml(url1);
        lireXml.importerXml(url2);
        lireCSV.importerCSV(url3);
    });
}
