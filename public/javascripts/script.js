

$(document).ready(function () {

// fonctionnalité A4
    $("#rechercheInstallation").click(function () {
        $("#listeinst").show();
        $("#infoinst").hide();
        
        var recherche = $("#recherche").val();
        var url = "/installations?arrondissement=" + recherche;
        // Appel alax pour obtenir la liste des installations
        $.get(url, function (donnees) {
            $("#donnees").html(afficherInstallations(donnees));
        }).fail(function (err) {
            $("#donnees").html("<tr><td colspan='2'>" + err.status + ":" + err.responseText + "</td></tr>");
            });
    });


    function afficherInstallations(listeInstallations) {
        var htmlString = '';
 
        listeInstallations.forEach(function (noeud) {
            htmlString += ajouterLigneInstallation(noeud);
            });
        return htmlString;
    }

    // fonctionnalité A5: route /installations retourne la liste de toute les installation sous forme de json
    // les noms des installations sont sauvegardées dans la liste deroulante
    $.get("/installations", function (donnees) {
        donnees.forEach(function (noeud) {
            var nomInstallation = obtenirNomInstallation(noeud);
            $("#listeinstallations").append('<option>' + nomInstallation + '</option>');
        });
    });

    $("#listeinstallations").change(function () {
        var installationChoisie = $("#listeinstallations").val();

        if (installationChoisie === 'choisir_installation') 
            $("#infoinst").hide();
        else {
            $("#listeinst").hide();
            $("#infoinst").show();
        }

        $.get("/installations", function (donnees) {
            $("#infoinst").html(afficherInfoInstallations(installationChoisie, donnees));
        }).fail(function (err) {
            $("#infoinst").html(afficherErreur(err));
        });
    });

    // fonctionnalité A6
    function afficherInfoInstallations(installationChoisie, listeInstallations) {
        var htmlString = '';

        for (var i = 0; i < listeInstallations.length; i++) {

            var tableauAjouter = '';

            // pour chaque istallation trouvée une table differente est créé et ajoutée au div #infoinst
            if (listeInstallations[i].hasOwnProperty('ID_UEV') && installationChoisie === listeInstallations[i].NOM) {
                htmlString += afficherInfoPiscine(listeInstallations[i]);
            } else if (listeInstallations[i].hasOwnProperty('arrose') && installationChoisie === listeInstallations[i].nom){
                htmlString += afficherInfoPatinoire(listeInstallations[i]);
            } else if (installationChoisie === listeInstallations[i].nom){
                htmlString += afficherInfoGlissade(listeInstallations[i]);
            }
        }
        return htmlString;
    }

    function obtenirNomInstallation(noeud) {

        if (noeud.hasOwnProperty('ID_UEV')) {
            return noeud.NOM;
        }
        else {
            return noeud.nom;
        }
    }

    function afficherInfoPiscine(noeud) {
        var table =  "<table class=tableInst><tr><th>ID_UEV</th>" +
                                                "<th>TYPE</th>" +
                                                "<th>NOM</th>" +
                                                "<th>ARRONDISSE</th>" +
                                                "<th>ADRESSE</th>" +
                                                "<th>PROPRIETE</th>" +
                                                "<th>GESTION</th>" +
                                                "<th>POINT_X</th>" +
                                                "<th>POINT_Y</th>" +
                                                "<th>EQUIPEME</th>" +
                                                "<th>LONG</th>" +
                                                "<th>LAT</th></tr > " + 
                                                "<tr><td>" + noeud.ID_UEV + "</td>" +
                                                    "<td>" + noeud.TYPE + "</td>" +
                                                    "<td>" + noeud.NOM + "</td>" +
                                                    "<td>" + noeud.ARRONDISSE + "</td>" +
                                                    "<td>" + noeud.ADRESSE + "</td>" +
                                                    "<td>" + noeud.PROPRIETE + "</td>" +
                                                    "<td>" + noeud.GESTION + "</td>" +
                                                    "<td>" + noeud.POINT_X + "</td>" +
                                                    "<td>" + noeud.POINT_Y + "</td>" +
                                                    "<td>" + noeud.EQUIPEME + "</td>" +
                                                    "<td>" + noeud.LONG + "</td>" +
                                                    "<td>" + noeud.LAT + "</td></tr ></table>";
        return table;
    }

    function afficherInfoGlissade(noeud) {
        var table = "<table class=tableInst><tr><th>Nom Installation</th>" +
                                                "<th>ouvert</th>" +
                                                "<th>deblaye</th>" +
                                                "<th>condition</th>" +
                                                "<th>Nom arrondissement</th>" +
                                                "<th>cle</th>" +
                                                "<th>date</th></tr>" +
                                                "<tr><td>" + noeud.nom + "</td>" +
                                                "<td>" + noeud.ouvert + "</td>" +
                                                "<td>" + noeud.deblaye + "</td>" +
                                                "<td>" + noeud.condition + "</td>" +
                                                "<td>" + noeud.arrondissement.nom_arr + "</td>" +
                                                "<td>" + noeud.arrondissement.cle + "</td>" +
                                                "<td>" + noeud.arrondissement.date_maj + "</td>" +
                                                "</tr ></table>";
        return table;
    }

    function afficherInfoPatinoire(noeud) {
       var table =   "<table class=tableInst><tr><th>Nom Installation</th>" +
                                                "<th>ouvert</th>" +
                                                "<th>deblaye</th>" +
                                                "<th>arrose</th>" +
                                                "<th>resurface</th>" +
                                                "<th>condition</th>" +
                                                "<th>Nom arrondissement</th>" +
                                                "<th>cle</th>" +
                                                "<th>date</th></tr>" +
                                                "<tr><td>" + noeud.nom + "</td>" +
                                                "<td>" + noeud.ouvert + "</td>" +
                                                "<td>" + noeud.deblaye + "</td>" +
                                                "<td>" + noeud.arrose + "</td>" +
                                                "<td>" + noeud.resurface + "</td>" +
                                                "<td>" + noeud.condition + "</td>" +
                                                "<td>" + noeud.arrondissement.nom_arr + "</td>" +
                                                "<td>" + noeud.arrondissement.cle + "</td>" +
                                                "<td>" + noeud.arrondissement.date_maj + "</td>" +
                                                "</tr ></table>";
        return table;
    }

    function afficherErreur(err) {
        return "<table><tr><th colspan='7'>Erreur</th></tr>" +
                      "<tr><td colspan='7'>" + err.status + " : " + err.responseText + "</td></tr></table>"
    }

    function ajouterLigneInstallation(noeud) {
        var ligne = "<tr><td>" + noeud._id + "</td>";

        if (noeud.hasOwnProperty('ID_UEV')) {
            ligne += "<td>" + noeud.NOM + "</td>" + "<td>" + noeud.ARRONDISSE + "</td></tr >";
        }
        else {
            ligne += "<td>" + noeud.nom + "</td>" + "<td>" + noeud.arrondissement.nom_arr + "</td></tr >";
        }
        return ligne;
    }

});



